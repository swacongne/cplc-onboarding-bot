# Setup instructions
create file auth.json with content

{
"token": "bot's private token"
}

npm install discord.js -save

# Run the bot
node bot.js

# Reference documentation
Discord.js events cheat sheet
https://gist.github.com/koad/316b265a91d933fd1b62dddfcc3ff584

Discord.js reference documentation
https://discord.js.org/#/docs/main/stable/general/welcome

Discord developer's portal
https://discordapp.com/developers/applications/

