#!/usr/bin/env node

const Discord = require('discord.js');
const auth = require('./auth.json');
const config = require('./config.json');
const client = new Discord.Client();

const findInMap = (map, val) => {
  for (let [k, v] of map) {
    if (v.name === val) { 
      return true; 
    }
  }  
  return false;
}

// if renameAtJoin active in config then set a nickname with a trailing "." at server join
client.on('guildMemberAdd', member => {
  if (config.renameAtJoin) {
    console.log('setting nickname for user '+member.user.username+": "+member.displayName+'.');
    member.setNickname(member.displayName+'.')
      .catch(console.error);
  }
});

// if renameAtRoleChange active in config then set a nickname with a trailing "." at role set
client.on('guildMemberUpdate', (oldMember, newMember) => {
  if (config.renameAtRoleChange) {
    if (findInMap(newMember.roles, config.membreRole) && !(findInMap(oldMember.roles, config.membreRole)) && !(newMember.displayName.endsWith('.'))) {
      console.log(newMember.displayName + ' has new role ' + config.membreRole + ': setting nickname for user '+newMember.user.username+" => "+newMember.displayName+'.');
      newMember.setNickname(newMember.displayName+'.')
        .catch(console.error);
    }
  }
});

// if the client's WebSocket encounters a connection error
client.on('error', error => {
  console.error('client WebSocket encountered a connection error: ' + error);
});

// emitted whenever the client tries to reconnect to the WebSocket. 
client.on('reconnecting', function(){
    console.log('client tries to reconnect to the WebSocket');
});

// emitted whenever a WebSocket resumes.
client.on('resume', replayed => {
    console.log('whenever a WebSocket resumes, ' + replayed + ' replays');
});

client.on('ready', () => {
   console.log('I am ready!'); 
});

client.login(auth.token);
